import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #I have to set up these os flags or else tensorflow does not work on my laptop
                                         #because I do not have an appropriate gpu, but you can probably comment them
                                         #out on your end
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import pathlib

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

data=os.path.join(os.getcwd(), "data")
data=pathlib.Path(data)

batch_size=32
h,w=180,180

#creating training dataset and validation dataset from the data directory with a 80/20 split
train_ds = tf.keras.utils.image_dataset_from_directory(
  data,
  label_mode='binary',
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(h,w),
  batch_size=batch_size)

val_ds = tf.keras.utils.image_dataset_from_directory(
  data,
  label_mode='binary',
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(h, w),
  batch_size=batch_size)

class_names=train_ds.class_names

AUTOTUNE = tf.data.AUTOTUNE

#preprocessing data
train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
val_ds = val_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)

model = Sequential([
  layers.Rescaling(1./255, input_shape=(w, h, 3)),
  layers.RandomRotation(0.1),
  layers.RandomZoom(0.1),
  #Adding random image modification layers to reduce overfitting
  layers.Conv2D(16, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Dropout(0.1),
  #Adding dropout layers after each convolution and dense layer to further reduce overfitting
  layers.Conv2D(32, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Dropout(0.1),
  layers.Conv2D(64, 3, padding='same', activation='relu'),
  layers.MaxPooling2D(),
  layers.Dropout(0.1),
  layers.Flatten(),
  layers.Dense(128, activation='relu'),
  layers.Dropout(0.1),
  layers.Dense(1, activation='sigmoid')])

model.compile(optimizer='adam',
              loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
              metrics=['accuracy'])

epochs=25 #Not too many epochs to avoid overfitting to the training dataset
history = model.fit( 
  train_ds,
  validation_data=val_ds,
  epochs=epochs
)

acc = history.history['accuracy']
val_acc = history.history['val_accuracy']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

#display results

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
print("test")
plt.show()

model.save(os.getcwd())