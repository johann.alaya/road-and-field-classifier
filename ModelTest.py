import matplotlib.pyplot as plt
import numpy as np
import os
import PIL
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' #I have to set up these os flags or else tensorflow does not work on my laptop
                                         #because I do not have an appropriate gpu, but you can probably comment them
                                         #out on your end
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import pathlib

from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential

path=os.path.join(os.getcwd(), "saved_model.pb")
model = keras.models.load_model(path)

road_url= "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/Dawn_on_the_great_alpine_road.jpg/220px-Dawn_on_the_great_alpine_road.jpg"
road_path = tf.keras.utils.get_file('australian_road', origin=road_url)

img = tf.keras.utils.load_img(
    road_path, target_size=(img_height, img_width)
)
img_array = tf.keras.utils.img_to_array(img)
img_array = tf.expand_dims(img_array, 0) # Create a batch

predictions = model.predict(img_array)
score = tf.nn.softmax(predictions[0])

print(
    "This image most likely belongs to {} with a {:.2f} percent confidence."
    .format(class_names[np.argmax(score)], 100 * np.max(score))
)

